# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import argparse
import logging
from time import sleep
from typing import List

from inscrawler import InsCrawler
from inscrawler.api.dtos.post_dto import PostDTO
from inscrawler.api.post_api import create_or_update_post
from inscrawler.api.profile_api import get_profiles_to_crawl
from inscrawler.constants.consts import RETRY_TIMEOUT, N_POSTS_TO_CRAWL, MAX_RETRY_TIMEOUT
from inscrawler.model.post import Post
from inscrawler.settings import override_settings
from inscrawler.settings import prepare_override_settings
from logger import start_logger


def usage():
    return """
        python main.py
        python main.py --debug
    """


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Instagram Crawler - Post", usage=usage())

    parser.add_argument("--debug", action="store_true")

    prepare_override_settings(parser)

    args = parser.parse_args()

    override_settings(args)

    start_logger()

    ins_crawler = InsCrawler(has_screen=args.debug)
    ins_crawler.login()

    retries = 1
    while True:
        try:
            logging.info("Getting list of profiles to crawl...")
            profiles_to_crawl: List[str] = get_profiles_to_crawl()
            logging.info(
                f'List of awaiting(to be crawled) profiles returned. - Profiles : {{{", ".join(profiles_to_crawl)}}}')
            retries = 1

            for profile_to_crawl in profiles_to_crawl:
                try:
                    ins_crawler.dismiss_login_prompt()
                    logging.info(f"Crawling posts of \'{profile_to_crawl}\'...")
                    posts: List[Post] = ins_crawler.get_posts(profile_to_crawl, N_POSTS_TO_CRAWL)

                    create_post_retries = 1
                    for post in posts:
                        try:
                            logging.info(f"Saving post: \'{post.url}\'...")
                            create_or_update_post(
                                PostDTO(username=profile_to_crawl, url=post.url, url_imgs=post.url_imgs,
                                        post_date=post.post_date,
                                        caption=post.caption,
                                        likes=post.likes))
                            logging.info(f"Post saved: \'{post.url}\'!")
                        except Exception as e:
                            logging.exception(e)
                            posts.append(post)
                            sleep(min(create_post_retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
                            create_post_retries += 1

                except Exception as e:
                    logging.exception(e)
                    sleep(min(retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
                    retries += 1

        except Exception as e:
            logging.exception(e)
            sleep(min(retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
            retries += 1
