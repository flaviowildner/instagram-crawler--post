from __future__ import unicode_literals

import glob
import logging
import os
import sys
import time
import traceback
from builtins import open
from typing import List

from tqdm import tqdm

from .browser import Browser
from .constants import consts
from .exceptions import RetryException
from .fetch import fetch_caption
from .fetch import fetch_datetime
from .fetch import fetch_imgs
from .model.post import Post
from .utils import retry


class Logging(object):
    PREFIX = "instagram-crawler"

    def __init__(self):
        try:
            timestamp = int(time.time())
            self.cleanup(timestamp)
            self.logger = open("/tmp/%s-%s.log" %
                               (Logging.PREFIX, timestamp), "w")
            self.log_disable = False
        except Exception:
            self.log_disable = True

    def cleanup(self, timestamp):
        days = 86400 * 7
        days_ago_log = "/tmp/%s-%s.log" % (Logging.PREFIX, timestamp - days)
        for log in glob.glob("/tmp/instagram-crawler-*.log"):
            if log < days_ago_log:
                os.remove(log)

    def log(self, msg):
        if self.log_disable:
            return

        self.logger.write(msg + "\n")
        self.logger.flush()

    def __del__(self):
        if self.log_disable:
            return
        self.logger.close()


class InsCrawler(Logging):
    URL = "https://www.instagram.com"
    RETRY_LIMIT = 10

    def __init__(self, has_screen: bool = False):
        super(InsCrawler, self).__init__()
        self.browser = Browser(has_screen)
        self.page_height = 0

    def dismiss_login_prompt(self):
        ele_login = self.browser.find_one(".Ls00D .Szr5J")
        if ele_login:
            ele_login.click()

    def login(self):
        browser = self.browser
        url = "%s/accounts/login/" % InsCrawler.URL
        browser.get(url)
        u_input = browser.find_one('input[name="username"]')
        u_input.send_keys(consts.USERNAME)
        p_input = browser.find_one('input[name="password"]')
        p_input.send_keys(consts.PASSWORD)

        login_btn = browser.find_one(".L3NKy")
        login_btn.click()

        @retry()
        def check_login():
            if browser.find_one('input[name="username"]'):
                raise RetryException()

        check_login()

    def get_posts(self, username, num):
        @retry()
        def check_next_post(cur_key):
            ele_a_datetime = browser.find_one("._aat8 .oajrlxb2.g5ia77u1.qu0x051f.esr5mh6w")

            # It takes time to load the post for some users with slow network
            if ele_a_datetime is None:
                raise RetryException()

            next_key = ele_a_datetime.get_attribute("href")
            if cur_key == next_key:
                raise RetryException()

        browser = self.browser
        url = "%s/%s/" % (InsCrawler.URL, username)
        browser.get(url)

        browser.implicitly_wait(1)
        browser.scroll_down()
        ele_post = browser.find_one("._aabd._aa8k._aanf a")

        # Return empty list for users without posts
        if ele_post is None:
            return list()

        ele_post.click()
        posts: List[Post] = []

        pbar = tqdm(total=num)
        pbar.set_description("fetching")
        cur_key = None

        # Fetching all posts
        for _ in range(num):
            post: Post = Post()

            # Fetching post detail
            try:
                check_next_post(cur_key)

                # Fetching datetime and url as key
                ele_a_datetime = browser.find_one("._aat8 .oajrlxb2.g5ia77u1.qu0x051f.esr5mh6w")
                cur_key = ele_a_datetime.get_attribute("href")
                post.url = cur_key
                fetch_datetime(browser, post)
                fetch_imgs(browser, post)
                fetch_caption(browser, post)

                try:
                    ele_img_before_likes = browser.find_one("._aacl._aaco._aacw._aacx._aada._aade img._aa8j")
                    qnt_likes = int(browser.find_one("._aacl._aaco._aacw._aacx._aada._aade > span").text.replace(",", "").replace(".", ""))

                    if ele_img_before_likes is not None:
                        qnt_likes += 1

                    post.likes = qnt_likes
                except Exception:
                    logging.warning(f"Failed at getting number of likes of \'{post.url}\'")


            except RetryException:
                sys.stderr.write(
                    "\x1b[1;31m" +
                    "Failed to fetch the post: " +
                    cur_key or 'URL not fetched' +
                    "\x1b[0m" +
                    "\n"
                )

                break

            except Exception:
                sys.stderr.write(
                    "\x1b[1;31m" +
                    "Failed to fetch the post: " +
                    cur_key if isinstance(cur_key, str) else 'URL not fetched' +
                                                             "\x1b[0m" +
                                                             "\n"
                )
                traceback.print_exc()

            # self.log(json.dumps(post, ensure_ascii=False))

            posts.append(post)

            pbar.update(1)
            right_arrow = browser.find_one("._aaqg._aaqh > ._abl-")
            if right_arrow:
                right_arrow.click()

        pbar.close()
        if posts:
            posts.sort(key=lambda post: post.post_date, reverse=True)

        return posts
