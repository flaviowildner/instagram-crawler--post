from typing import List


class PostDTO:
    username: str
    url: str
    url_imgs: List[str]
    post_date: int
    caption: str
    likes: int

    def __init__(self, username=None, url=None, url_imgs=None, post_date=None, caption=None, likes=None) -> None:
        self.username = username
        self.url = url
        self.url_imgs = url_imgs
        self.post_date = post_date
        self.caption = caption
        self.likes = likes
