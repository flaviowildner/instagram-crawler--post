import logging

import requests

from inscrawler.api.dtos.post_dto import PostDTO
from inscrawler.constants.consts import ENTITY_URL


def create_or_update_post(post: PostDTO):
    response = requests.put(f'{ENTITY_URL}/post',
                            json=post.__dict__)
    if not response.ok:
        logging.error(f'Error while saving post. - ${post.url}')
