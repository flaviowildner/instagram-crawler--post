from typing import List

import requests

from inscrawler.constants.consts import ENTITY_URL, SERVICE_IDENTIFIER


def get_profiles_to_crawl() -> List[str]:
    response = requests.get(f'{ENTITY_URL}/profile/get_profiles_to_crawl',
                            params={'n_profiles': 10, 'service_identifier': SERVICE_IDENTIFIER})
    if response.status_code != 200:
        raise Exception(f'Error when getting the profile list from Entity API - {response.json()}')

    return response.json()


def get_or_create_profile(username: str):
    response = requests.get(f'{ENTITY_URL}/profile/{username}')
    return response.json()
