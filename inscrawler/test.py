from inscrawler.model.comment import Comment
from inscrawler.persistence.data.comment_data import to_entity
from inscrawler.persistence.entity import ProfileEntity, PostEntity, CommentEntity

profile = ProfileEntity.get(id=1)
post = PostEntity.get(id=1)

# comment_entity = CommentEntity.create()
comment = Comment(id_=1, post=post, author=profile, comment="testeetas", last_visit=None, comment_date=None, deleted=None)

comment_entity = to_entity(comment)

comment_entity.save()

