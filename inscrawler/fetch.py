import re
from time import sleep
from typing import List

import dateutil.parser

from .api.profile_api import get_or_create_profile
from .exceptions import RetryException
from .model.comment import Comment
from .model.post import Post
from .settings import settings
from .utils import retry


def get_parsed_mentions(raw_text):
    regex = re.compile(r"@([\w\.]+)")
    regex.findall(raw_text)
    return regex.findall(raw_text)


def get_parsed_hashtags(raw_text):
    regex = re.compile(r"#(\w+)")
    regex.findall(raw_text)
    return regex.findall(raw_text)


def fetch_mentions(raw_test, dict_obj):
    if not settings.fetch_mentions:
        return

    mentions = get_parsed_mentions(raw_test)
    if mentions:
        dict_obj["mentions"] = mentions


def fetch_hashtags(raw_test, dict_obj):
    if not settings.fetch_hashtags:
        return

    hashtags = get_parsed_hashtags(raw_test)
    if hashtags:
        dict_obj["hashtags"] = hashtags


def fetch_datetime(browser, post: Post):
    ele_datetime = browser.find_one("._aacl._aacm._aacu._aacy._aad6 > time._aaqe")
    datetime = ele_datetime.get_attribute("datetime")

    post.post_date = int(dateutil.parser.parse(datetime).timestamp())


def fetch_imgs(browser, post: Post):
    img_urls = set()
    while True:
        # post with single media
        ele_imgs = browser.find("._aagu._aato > ._aagv img", waittime=10)

        if ele_imgs is None:
            # post with multiple medias
            ele_imgs = browser.find("._aagu._aamh > ._aagv img", waittime=10)

        if isinstance(ele_imgs, list):
            for ele_img in ele_imgs:
                img_urls.add(ele_img.get_attribute("src"))
        else:
            break

        next_photo_btn = browser.find_one("button._aahi")

        if next_photo_btn:
            next_photo_btn.click()
            sleep(0.3)
        else:
            break

    post.url_imgs = list(img_urls)


# def fetch_likers(browser, post: Post):
#     if not settings.fetch_likers:
#         return
#
#     like_info_btn = browser.find_one(".EDfFK .sqdOP._8A5w5 span")
#     if like_info_btn is not None:
#         like_info_btn.click()
#
#         likers = {}
#         liker_elems_css_selector = ".Igw0E ._7UhW9.xLCgt a"
#         likers_elems = list(browser.find(liker_elems_css_selector))
#         last_liker = None
#         while likers_elems:
#             for ele in likers_elems:
#                 likers[ele.get_attribute("href")] = ele.get_attribute("title")
#
#             if last_liker == likers_elems[-1]:
#                 break
#
#             last_liker = likers_elems[-1]
#             last_liker.location_once_scrolled_into_view
#             sleep(0.6)
#             likers_elems = list(browser.find(liker_elems_css_selector))
#
#         post.likers = list([get_or_create_profile(username) for username in likers.values()])
#         close_btn = browser.find_one(".WaOAr button")
#         close_btn.click()


def fetch_caption(browser, post: Post):
    ele_comments = browser.find("._a9zj._a9zl._a9z5 span._aacl._aaco._aacu._aacx._aad7._aade")

    if len(ele_comments) > 0:
        for element in ele_comments:
            if element.text not in ['Verified', '']:
                post.caption = element.text


def fetch_comments(browser, post: Post):
    if not settings.fetch_comments:
        return

    @retry()
    def check_next_likers():
        ele_a_datetime = browser.find_one(".eo2As .c-Yi7")

        # It takes time to load the post for some users with slow network
        if ele_a_datetime is None:
            raise RetryException()

    show_more_selector = "button .glyphsSpriteCircle_add__outline__24__grey_9"
    show_more = browser.find_one(show_more_selector)
    while show_more:
        show_more.location_once_scrolled_into_view
        show_more.click()
        sleep(0.3)
        show_more = browser.find_one(show_more_selector)

    show_comment_btns = browser.find(".EizgU")
    for show_comment_btn in show_comment_btns:
        show_comment_btn.location_once_scrolled_into_view
        show_comment_btn.click()
        sleep(0.3)

    ele_comments = browser.find(".eo2As .gElp9")
    comments: List[Comment] = []
    for els_comment in ele_comments[1:]:
        comment_obj: Comment = Comment()

        author = browser.find_one(".sqdOP.yWX7d._8A5w5.ZIAjV", els_comment).text
        # author = browser.find_one(".FPmhX", els_comment).text
        comment_obj.author = get_or_create_profile(author)

        comment_obj.likers = list()
        if settings.fetch_likers:
            likers_btn = browser.find_one('._7UhW9 button', els_comment)
            if ('like' in likers_btn.text or 'curtida' in likers_btn.text):
                likers_btn.click()

                likers = {}
                liker_elems_css_selector = ".Igw0E ._7UhW9.xLCgt a"
                likers_elems = list(browser.find(liker_elems_css_selector))
                last_liker = None
                while likers_elems:
                    for ele in likers_elems:
                        likers[ele.get_attribute(
                            "href")] = ele.get_attribute("title")

                    if last_liker == likers_elems[-1]:
                        break

                    last_liker = likers_elems[-1]
                    last_liker.location_once_scrolled_into_view
                    sleep(0.6)
                    likers_elems = list(browser.find(liker_elems_css_selector))

                comment_obj.likers = list([get_or_create_profile(username) for username in likers.values()])

                close_btn = browser.find_one(".WaOAr button")
                close_btn.click()

        comment_element = browser.find("span", els_comment)
        for element in comment_element:
            if element.text not in ['Verified', '']:
                comment = element.text
                comment_obj.comment = comment

        time_element = browser.find_one("time", els_comment)
        datetime = time_element.get_attribute("datetime")
        comment_obj.comment_date = int(dateutil.parser.parse(datetime).timestamp())

        # TODO
        # fetch_mentions(comment, comment_obj)
        # fetch_hashtags(comment, comment_obj)

        comments.append(comment_obj)

    post.comments = comments


def fetch_initial_comment(browser, dict_post):
    comments_elem = browser.find_one("ul.XQXOT")
    first_post_elem = browser.find_one(".ZyFrc", comments_elem)
    caption = browser.find_one("span", first_post_elem)

    if caption:
        dict_post["description"] = caption.text


def fetch_details(browser, dict_post):
    if not settings.fetch_details:
        return

    browser.open_new_tab(dict_post["key"])

    username = browser.find_one("a.ZIAjV")
    location = browser.find_one("a.O4GlU")

    if username:
        dict_post["username"] = username.text
    if location:
        dict_post["location"] = location.text

    fetch_initial_comment(browser, dict_post)

    browser.close_current_tab()
