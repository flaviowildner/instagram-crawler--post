import os

USERNAME = os.environ.get('USERNAME', '')
PASSWORD = os.environ.get('PASSWORD', '')

RETRY_TIMEOUT = int(os.environ.get('RETRY_TIMEOUT', 10))
MAX_RETRY_TIMEOUT = int(os.environ.get('MAX_RETRY_TIMEOUT', 60))
N_POSTS_TO_CRAWL = int(os.environ.get('N_POSTS_TO_CRAWL', 60))

ENTITY_URL = os.environ.get('ENTITY_URL', '')
