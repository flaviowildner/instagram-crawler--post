future==0.16.0
selenium==3.9.0
tqdm==4.23.4
pre-commit==1.16.1
python-dateutil==2.8.1
requests~=2.25.1
webdriver-manager==3.4.2