#!/bin/bash
set -e
python3 -m virtualenv venv
. venv/bin/activate
pip install -r requirements.txt

python main.py